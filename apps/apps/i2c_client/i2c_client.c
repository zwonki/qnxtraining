/* For "devctl()" */
#include <devctl.h>
#include <hw/i2c.h>

/* For "open()" */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>


int read_data(int fd, uint8_t slave, int start, int size, uint8_t data[])
{
    int error = 0;

    i2c_sendrecv_t hdr;
    hdr.slave.addr = slave;
    hdr.slave.fmt = I2C_ADDRFMT_7BIT;
    hdr.send_len = 1;
    hdr.recv_len = size;    
    hdr.stop = 0;
    
    struct io_t
    {
        i2c_sendrecv_t hdr;
        uint8_t data[255];
    } frame;

    frame.hdr = hdr;
    frame.data[0] = start;     

    if(error = devctl (fd, DCMD_I2C_SENDRECV, &frame, sizeof(frame), NULL))
    {
        printf("Error devctl: %s\n", strerror ( error ));
        return -1;
    }
    
    for(int i=0; i<size; i++)
    {
        data[i] = frame.data[i];
    }

    return size;
}


int write_byte(int fd, uint8_t slave, uint8_t address, uint8_t value)
{
    int error = 0;

    i2c_sendrecv_t hdr;
    hdr.slave.addr = slave;
    hdr.slave.fmt = I2C_ADDRFMT_7BIT;
    hdr.send_len = 2;
    hdr.recv_len = 0;  
    hdr.stop = 0;
    
    struct io_t
    {
        i2c_sendrecv_t hdr;
        uint8_t data[2];
    } frame;

    frame.hdr = hdr;
    frame.data[0] = address;
    frame.data[1] = value;

    if(error = devctl(fd, DCMD_I2C_SENDRECV, &frame, sizeof(frame), NULL))
    {
        printf("NOK - ");
        printf("Error devctl: %s\n", strerror ( error ));
        return -1;
    }
    
    return 0;
}


void exit_reset(int fd, uint8_t slave)
{
    int error = write_byte(fd, slave, 0, 0);

    if(error)
    {
        printf("NOK - ");
        printf("Error devctl: %s\n", strerror ( error ));
    }
}

void read_time(int fd, uint8_t address)
{
    uint8_t data[3];

    int status = read_data(fd, address, 2, 3, data);

    if(status == -1)
    {
        printf("NOK - \n");
    }
    else
    {
        printf("OK - time: %02x:%02x:%02x\n", data[2] & 0x3f, data[1] & 0x7f, data[0] & 0x7f);
    }
}


int main(void)
{
    int data = 0, fd, error;

    if((fd = open("/dev/i2c1", O_RDWR)) == -1)
    {
        fprintf(stderr, "Error with open() on /dev/i2c1.  Make sure it exists.\n");
        perror(NULL);
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<128; i++)
    { 
        uint8_t data[2] = {0};

        printf("Scaning for: 0x%02x - ", i);   
        int status = read_data(fd, i, 0, 2, data);  

        if(status == -1)
        {
            continue;
        }
        else
        {
            printf("OK - status: %02x:%02x\n", data[0], data[1]);
        }

        exit_reset(fd, i);
        read_time(fd, i);
    }

    close(fd);

    return (1);
}

