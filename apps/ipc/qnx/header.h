#ifndef HEADER_H
#define HEADER_H

#include <sys/dispatch.h>

#define ATTACH_POINT "myname"

#define PID_PATH "/tmp/pid"

/* We specify the header as being at least a pulse */
typedef struct _pulse msg_header_t;

/* Our real data comes after the header */
typedef struct _my_data {
    msg_header_t hdr;
    int data;
} my_data_t;

#endif
