#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/neutrino.h>

int main(void)
{
  char *smsg = "This is the outgoing buffer";
  char rmsg [200];
  int  coid;
  
  // establish a connection
  coid = ConnectAttach (0, 495642, 1, 0, 0);
  if (coid == -1) {
      fprintf (stderr, "Couldn't ConnectAttach to 0/77/1!\n");
      perror (NULL);
      exit (EXIT_FAILURE);
  }
  
  // send the message
  if (MsgSend (coid,
               smsg, 
               strlen (smsg) + 1, 
               rmsg, 
               sizeof (rmsg)) == -1) {
      fprintf (stderr, "Error during MsgSend\n");
      perror (NULL);
      exit (EXIT_FAILURE);
  }
  
  if (strlen (rmsg) > 0) {
      printf ("Process ID 77 returns \"%s\"\n", rmsg);
  }

  return 0;
}
