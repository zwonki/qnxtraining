#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/neutrino.h>
#include "header.h"

int main(void)
{
  char *smsg = "This is the outgoing buffer";
  char rmsg [200];
  int  coid;
  int  pid = -1;
  
  FILE *f = fopen(PID_PATH, "r");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }

  fscanf(f, "%d", &pid);
  fclose(f);
  
  // establish a connection
  coid = ConnectAttach (0, pid, 1, 0, 0);
  if (coid == -1) {
      fprintf (stderr, "Couldn't ConnectAttach to 0/%d/1!\n",pid);
      perror (NULL);
      exit (EXIT_FAILURE);
  }
  
  // send the message
  if (MsgSend (coid,
               smsg, 
               strlen (smsg) + 1, 
               rmsg, 
               sizeof (rmsg)) == -1) {
      fprintf (stderr, "Error during MsgSend\n");
      perror (NULL);
      exit (EXIT_FAILURE);
  }
  
  if (strlen (rmsg) > 0) {
      printf ("Process ID %d returns \"%s\"\n", pid, rmsg);
  }

  return 0;
}
