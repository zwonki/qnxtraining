#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main(int argc, char *argv[])
{
   int fd;
   char buf[256];
   int num_bytes;

   fd = open( "/pps/example/button?wait,delta", O_RDONLY );
   if ( fd < 0 )
   {
      perror ("Couldn't open /pps/example/button");
      return EXIT_FAILURE;
   }

   /* Loop, echoing the attributes of the button. */
   while (1)
   {
      num_bytes = read( fd, buf, sizeof(buf) );
      if (num_bytes > 0)
      {
        write (STDOUT_FILENO, buf, num_bytes);
      }
   }

   return EXIT_SUCCESS;
}
