#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[])
{
   int fd;
   int color = 0;
   int count = 0;
   char buf[256];
   struct stat stat_buf;
   ssize_t len, bytes_written;

   /* Is PPS running? */
   if (stat( "/pps", &stat_buf) != 0)
   {
      if (errno == ENOENT)
         printf ("The PPS server isn't running.\n");
      else
         perror ("stat (/pps)");
      return EXIT_FAILURE;
   }

   /* Create the "button" object (if it doesn't already exist). */
   fd = open( "/pps/example/button", O_RDWR | O_CREAT, S_IRWXU | S_IRWXG | S_IRWXO );
   if ( fd < 0 )
   {
      perror ("Couldn't open /pps/example/button");
      return EXIT_FAILURE;
   }

   /* Loop forever, changing the color. */
   while ( 1 )
   {
      usleep (300);
      count++;
      len = snprintf(buf, 256, "color::%s\npub2::%d", color ? "red" : "green", count);
      bytes_written = write( fd, buf, len );
      if (bytes_written == -1)
      {
          perror ("write()");
      }
      else if (bytes_written != len)
      {
          printf ("Bytes written: %d String length: %d\n", bytes_written, len);
      }

      if ( color == 0 )
          color = 1;
      else
         color = 0;
   }

   return EXIT_SUCCESS;
}
