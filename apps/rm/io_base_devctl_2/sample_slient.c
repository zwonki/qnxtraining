/* For "devctl()" */
#include "header.h"

/* For "open()" */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

void print_value(int fd)
{
    int error = 0;

    data_t value = {-1, -1};

		if (error = devctl (fd,MY_DEVCTL_GETVAL , &value, sizeof(value), NULL))
    {
			printf("NOK - ");
    	printf("Error devctl: %s\n", strerror ( error )); 
    }
		else
		{
			printf(" OK - data: %d\n", value.rx);
		}
}

int main(void)
{
    int data = 0, fd, error;

    if((fd = open ("/dev/devctl_example", O_RDWR)) == -1)
    {
     fprintf(stderr, "Error with open().  Make sure it exists.\n");
     perror (NULL);
     exit(EXIT_FAILURE);
    }

    print_value(fd);

    data_t value = {.tx = 123, .rx = -1};
	
    if (error = devctl (fd,MY_DEVCTL_SETVAL , &value, sizeof(value), NULL))
    {
			printf("NOK - ");
    	printf("Error devctl: %s\n", strerror ( error )); 
    }
		else
		{
			printf(" OK - data: %d\n", value.rx);
		}
		
    print_value(fd);

    value.tx = 234;
    
    if (error = devctl (fd,MY_DEVCTL_SETGET , &value, sizeof(value), NULL))
    {
			printf("NOK - ");
    	printf("Error devctl: %s\n", strerror ( error )); 
    }
		else
		{
			printf(" OK - data: %d\n", value.rx);
		}
    
    print_value(fd);

    close(fd);

    return (1);
}

