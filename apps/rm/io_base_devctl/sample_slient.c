/* For "devctl()" */
#include "header.h"

/* For "open()" */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

int main(void)
{
    int data = 0, fd, error;

    if((fd = open ("/dev/devctl_example", O_RDWR)) == -1)
    {
     fprintf(stderr, "Error with open().  Make sure it exists.\n");
     perror (NULL);
     exit(EXIT_FAILURE);
    }


    int value = -1;

		if (error = devctl (fd, MY_DEVCTL_SETGET, &value, sizeof(value), NULL))
    {
			printf("NOK - ");
    	printf("Error devctl: %s\n", strerror ( error )); 
    }
		else
		{
			printf(" OK - data: %02x\n", value);
		}



    close(fd);

    return (1);
}

