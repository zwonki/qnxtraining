/* For "devctl()" */
#include <devctl.h>
#include <hw/i2c.h>

/* For "open()" */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "header.h"

int main(void)
{
    int fd, error;
    
    if((fd = open ("/dev/i2c1/68", O_RDWR)) == -1)
    {
     fprintf(stderr, "Error with open() on /dev/i2c1.  Make sure it exists.\n");
     perror (NULL);
     exit(EXIT_FAILURE);
    }

    data_t data;
    data.addr = 0;
    data.len = 5;
    memset(data.buffer, 0, 25);
		
		if (error = devctl (fd, MY_DEVCTL_READ,  &data, sizeof(data_t), NULL))
    {
			printf("NOK - ");
    	printf("Error devctl: %s\n", strerror ( error )); 
    }
		else
		{
      for(int i=0; i < 5; i++)
        printf("%02x", data.buffer[i]);
      printf("\n");
			//printf(" OK - data: %02x:%02x:%02x %02x %02x\n", data.buffer[0],data.buffer[1],data.buffer[2], data.buffer[3], data.buffer[4]);
		}

    close(fd);

    return (0);
}

