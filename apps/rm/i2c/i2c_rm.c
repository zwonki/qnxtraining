#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/iofunc.h>
#include <sys/dispatch.h>

#include "header.h"
#include "bcm2711/proto.h"

///////////////////////////////////////////////////////////

int io_read (resmgr_context_t *ctp, io_read_t *msg, RESMGR_OCB_T *ocb);
int io_write (resmgr_context_t *ctp, io_write_t *msg, RESMGR_OCB_T *ocb);
int io_devctl(resmgr_context_t *ctp, io_devctl_t *msg, RESMGR_OCB_T *ocb);

static resmgr_connect_funcs_t   connect_funcs;
static resmgr_io_funcs_t        io_funcs;
static iofunc_attr_t            attr;

static void* hdlLocal;

static int id_68 = 0;
static int id_69 = 0;


int main(int argc, char **argv)
{
    /* declare variables we'll be using */
    resmgr_attr_t        resmgr_attr;
    dispatch_t           *dpp;
    dispatch_context_t   *ctp;

    hdlLocal = bcm2711_init(0,NULL);
    bcm2711_set_bus_speed(hdlLocal, 100000, NULL);

    bcm2711_set_slave_addr(hdlLocal, 0x68, I2C_ADDRFMT_7BIT);

    /* initialize dispatch interface */
    if((dpp = dispatch_create()) == NULL) {
        fprintf(stderr, "%s: Unable to allocate dispatch handle.\n",
                argv[0]);
        return EXIT_FAILURE;
    }

    /* initialize resource manager attributes */
    memset(&resmgr_attr, 0, sizeof resmgr_attr);
    resmgr_attr.nparts_max = 1;
    resmgr_attr.msg_max_size = 2048;

    /* initialize functions for handling messages, including
       our read handlers */
    iofunc_func_init(_RESMGR_CONNECT_NFUNCS, &connect_funcs,_RESMGR_IO_NFUNCS, &io_funcs);
    io_funcs.read = io_read;
    io_funcs.read64 = io_read;
    io_funcs.write = io_write; 
    io_funcs.devctl = io_devctl;
 
    /* initialize attribute structure used by the device */
    iofunc_attr_init(&attr, S_IFNAM | 0666, 0, 0);
    attr.nbytes = sizeof(data_t);
    
    /* attach our device name */
    if((id_68 = resmgr_attach(dpp, &resmgr_attr, "/dev/i2c1/68", _FTYPE_ANY, 0, &connect_funcs, &io_funcs, &attr)) == -1) 
    {
        fprintf(stderr, "%s: Unable to attach name.\n", argv[0]);
        return EXIT_FAILURE;
    }
    if((id_69 = resmgr_attach(dpp, &resmgr_attr, "/dev/i2c1/69", _FTYPE_ANY, 0, &connect_funcs, &io_funcs, &attr)) == -1) 
    {
        fprintf(stderr, "%s: Unable to attach name.\n", argv[0]);
        return EXIT_FAILURE;
    }

    /* allocate a context structure */
    ctp = dispatch_context_alloc(dpp);

    /* start the resource manager message loop */
    while(1) 
    {
        if((ctp = dispatch_block(ctp)) == NULL) {
            fprintf(stderr, "block error\n");
            return EXIT_FAILURE;
        }
        dispatch_handler(ctp);
    }

    bcm2711_fini(hdlLocal);
    return EXIT_SUCCESS;
}

int io_read (resmgr_context_t *ctp, io_read_t *msg, RESMGR_OCB_T *ocb)
{
    return (ENOSYS);
}

int io_write (resmgr_context_t *ctp, io_write_t *msg, RESMGR_OCB_T *ocb)
{
    return(ENOSYS);
}


int io_devctl(resmgr_context_t *ctp, io_devctl_t *msg, RESMGR_OCB_T *ocb) {
    int     nbytes, status, previous;
    char buf[1] = {0};
   
    if ((status = iofunc_devctl_default(ctp, msg, ocb)) != _RESMGR_DEFAULT) {
        return(status);
    }
    status = nbytes = 0;

    /* Get the data from the message. */
    data_t *i2cData = _DEVCTL_DATA(msg->i);

    if(ctp->id == id_68)
      printf("ID 68: %d\n", ctp->id);
    
    if(ctp->id == id_69)
      printf("ID 69: %d\n", ctp->id);
    
    
    switch (msg->i.dcmd) {
    case MY_DEVCTL_READ:     
      buf[0] = i2cData->addr;

      status = bcm2711_send(hdlLocal, buf, 1, 0);
      if (status != I2C_STATUS_DONE) {
        //error
      }
      status = bcm2711_recv(hdlLocal, i2cData->buffer, i2cData->len, 0);
      nbytes = sizeof(data_t);      
      break;

    default:
        return(ENOSYS); 
    }

    /* Clear the return message. Note that we saved our data past
       this location in the message. */
    memset(&msg->o, 0, sizeof(msg->o));

    /*
     If you wanted to pass something different to the return
     field of the devctl() you could do it through this member.
    */
    msg->o.ret_val = status;

    /* Indicate the number of bytes and return the message */
    msg->o.nbytes = nbytes;
    return(_RESMGR_PTR(ctp, &msg->o, sizeof(msg->o) + nbytes));
}

