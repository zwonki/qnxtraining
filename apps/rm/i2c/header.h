#ifndef HEADER_H
#define HEADER_H

#include <devctl.h>

typedef struct _my_devctl_msg {
        char addr;             /* Filled by client on send */
        char len;             /* Filled by server on reply */
        char buffer[25];
} data_t;


#define MY_CMD_CODE      1
#define MY_DEVCTL_READ __DIOTF(_DCMD_MISC,  MY_CMD_CODE + 0, data_t)

#endif
